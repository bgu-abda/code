### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ 5b053e3c-8666-11eb-1c71-b15f59af900f
using Turing, Distributions, Statistics, StatsPlots, Random

# ╔═╡ df6d502e-8666-11eb-010d-e37085f2975b
md"# The Distribution Zoo"

# ╔═╡ 716c745c-866f-11eb-2368-e58243cc1d63
md"# Part A: Discrete Distributions"

# ╔═╡ e8e81aa8-8666-11eb-3525-0196d7519e95
md"## Bernoulli(p) distribution

1 with probability p and 0 with probability 1-p.
"

# ╔═╡ 18a6dfcc-8667-11eb-2266-a3c56b82f710
let x = rand(Bernoulli(0.7), 1000)
	histogram(x, normalize=true, bins=1)
end

# ╔═╡ 86b599d6-8667-11eb-3001-61ba1d6dd59c
md"### Example problem — coin flip
"

# ╔═╡ 9e2ee4dc-8667-11eb-18bf-e5b6c29de8f4
@model function coinflip(y::Array{Int,1})
	p ~ Beta(1, 1)
	for i = eachindex(y)
		y[i] ~ Bernoulli(p)
	end
end

# ╔═╡ ae23a116-866c-11eb-39f6-8fe27f23f42f
let chain = sample(coinflip(Array{Int, 1}([1, 1, 0, 1, 0, 0, 1, 1, 1])), MH(), 10000)
	histogram(chain[:p]), summarystats(chain[:p].data[:,1])
end

# ╔═╡ 9df78d72-866a-11eb-2250-ffc7cbb253fd
md"## Binomial(n, p) distribution

Number of successes out of N Bernoulli(p) trials. 
"

# ╔═╡ d5b5d8fe-866a-11eb-2f58-494821fed08e
let x = rand(Binomial(100, 0.3), 10000)
	histogram(x, normalize=true)
end

# ╔═╡ 6986d7a6-866b-11eb-1c57-1159f3742d01
md"### Example problem — coin flip 

Same problem again, but in collapsed form.
"

# ╔═╡ 9c9993de-866b-11eb-17ec-6b3caf28b3fb
@model function coinflips(n, k)
	p ~ Uniform(0, 1)
	k ~ Binomial(n, p)
end

# ╔═╡ d25a3fe0-866c-11eb-1d04-9760b98d2812
let chain = sample(coinflips(5, 3), MH(), 10000)
	histogram(chain[:p]), summarystats(chain[:p].data[:,1])
end

# ╔═╡ 0e894f24-866d-11eb-10cf-b182916dd887
md"## Poisson(λ) distribution

Number of independent events happening with rate  λ.
"

# ╔═╡ 8a894818-866d-11eb-0df7-d124a4859f42
let x = rand(Poisson(2), 1000)
	histogram(x, normalize=true)
end

# ╔═╡ c9de5cba-866d-11eb-1aa7-f9bc8ee0ea93
md"### Example problem — number of calls

Number of calls in the call center is recorded each hour. What is the distribution of the number of calls?
"

# ╔═╡ 210db242-866e-11eb-172a-af081f376d1d
@model function numcalls(counts::Array{Int,1})
	λ ~ Uniform(0, 10)
	for i in eachindex(counts)
		counts[i] ~ Poisson(λ)
	end
end

# ╔═╡ ab0df3bc-866e-11eb-1c95-cf163f4a82af
let chain = sample(numcalls([3, 5, 1, 4, 7]), HMC(0.1, 5), 1000)
	histogram(chain[:λ]), summarystats(chain[:λ].data[:,1])
end

# ╔═╡ 601a493c-8670-11eb-13c4-772dff23e922
md" # Part 2: Continuous distributions"

# ╔═╡ 11ec4b04-8671-11eb-173c-e39662c35dca
md"## Normal(μ, σ) distribution

The bell curve. Distribution of sum or average of many random variables.
"

# ╔═╡ 2af27128-8671-11eb-3368-2509cd09b3c7
let x = rand(Normal(0, 1), 10000)
	histogram(x, normalize=true)
end

# ╔═╡ abeb7764-8673-11eb-3756-c37f9ee930a7
md"### Example problem — height distribution
"

# ╔═╡ beff528a-8673-11eb-1495-6f4fcacc8b8d
@model function height(heights::Array{Float64,1})
	μ ~ Uniform(50, 300)
	σ ~ Uniform(10, 500)
	for i in eachindex(heights)
		heights[i] ~ Normal(μ, σ)
	end
end

# ╔═╡ 9d20eede-8674-11eb-0738-fb4f73888f75
let chain = sample(height([150., 165., 149., 177., 158.]), HMC(0.1, 5), 1000)
	histogram(chain[:μ], title="μ"), histogram(chain[:σ], title="σ"), summarystats(chain[:μ].data[:,1]), summarystats(chain[:σ].data[:, 1])
end

# ╔═╡ 2f17cf90-8675-11eb-1edf-79ce5dc96649
md"## Exponential(λ) distribution

Intervals between events in Poisson(λ) process.
"


# ╔═╡ 60c5cb9a-8675-11eb-02d2-81c49edd7eff
let x = rand(Exponential(100), 1000000)
	histogram(x, normalize=true)
end

# ╔═╡ f70c6080-8675-11eb-08d4-b70e84770690
md"### Example problem

Average time to wait for the next bus.
"

# ╔═╡ 349c1c38-8676-11eb-13dc-e184d1c5a920
@model function waitforbus(intervals)
	λ ~ Gamma(2, 30) # more on that later
	for i in 1:length(intervals)
		intervals[i] ~ Exponential(λ)
	end
end

# ╔═╡ 9b1eabce-8676-11eb-0e57-4d8c217c4509

let times = [0., 40., 70., 115., 165., 195.],
	intervals = times[begin+1:end] .- times[begin:end-1], 
	chain = sample(waitforbus(intervals), HMC(0.1, 5), 1000)
	histogram(chain[:λ]), summarystats(chain[:λ].data[:,1])
end

# ╔═╡ Cell order:
# ╟─df6d502e-8666-11eb-010d-e37085f2975b
# ╠═5b053e3c-8666-11eb-1c71-b15f59af900f
# ╟─716c745c-866f-11eb-2368-e58243cc1d63
# ╟─e8e81aa8-8666-11eb-3525-0196d7519e95
# ╠═18a6dfcc-8667-11eb-2266-a3c56b82f710
# ╟─86b599d6-8667-11eb-3001-61ba1d6dd59c
# ╠═9e2ee4dc-8667-11eb-18bf-e5b6c29de8f4
# ╠═ae23a116-866c-11eb-39f6-8fe27f23f42f
# ╠═9df78d72-866a-11eb-2250-ffc7cbb253fd
# ╠═d5b5d8fe-866a-11eb-2f58-494821fed08e
# ╠═6986d7a6-866b-11eb-1c57-1159f3742d01
# ╠═9c9993de-866b-11eb-17ec-6b3caf28b3fb
# ╠═d25a3fe0-866c-11eb-1d04-9760b98d2812
# ╟─0e894f24-866d-11eb-10cf-b182916dd887
# ╠═8a894818-866d-11eb-0df7-d124a4859f42
# ╟─c9de5cba-866d-11eb-1aa7-f9bc8ee0ea93
# ╠═210db242-866e-11eb-172a-af081f376d1d
# ╠═ab0df3bc-866e-11eb-1c95-cf163f4a82af
# ╟─601a493c-8670-11eb-13c4-772dff23e922
# ╠═11ec4b04-8671-11eb-173c-e39662c35dca
# ╠═2af27128-8671-11eb-3368-2509cd09b3c7
# ╟─abeb7764-8673-11eb-3756-c37f9ee930a7
# ╠═beff528a-8673-11eb-1495-6f4fcacc8b8d
# ╠═9d20eede-8674-11eb-0738-fb4f73888f75
# ╟─2f17cf90-8675-11eb-1edf-79ce5dc96649
# ╠═60c5cb9a-8675-11eb-02d2-81c49edd7eff
# ╠═f70c6080-8675-11eb-08d4-b70e84770690
# ╠═349c1c38-8676-11eb-13dc-e184d1c5a920
# ╠═9b1eabce-8676-11eb-0e57-4d8c217c4509
