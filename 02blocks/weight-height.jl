### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 3221b210-8716-11eb-08d0-d9254fad7c98
using CSV, DataFrames, Statistics, Plots, Distributions

# ╔═╡ 3e354594-8716-11eb-1e34-1f733bf7932d
df = CSV.read("weight-height.csv", DataFrame)

# ╔═╡ 11921312-8718-11eb-0c6f-c3c312d8c9f0
function viz(values::Array{Float64, 1})
	μ = mean(values)
	σ = std(values)
	p = histogram(values, normalize=true, label="data")
	x = range(minimum(values), stop=maximum(values), length=100)
	plot!(p, x, pdf.(Normal(μ, σ), x), width=3, label="normal pdf")
	return p
end	

# ╔═╡ 39cec4fe-871a-11eb-05e6-89d835d83388
let p = viz(df[in(["Male"]).(df.Gender), :Height])
	xlabel!("male height")
	savefig("male-height.svg")
	p
end

# ╔═╡ b0cc2484-871a-11eb-3f4b-3752b8a7776c
let p = viz(df[in(["Female"]).(df.Gender), :Weight])
	xlabel!(p, "female weight")
	savefig("female-weight.svg")
	p
end

# ╔═╡ bd9fd2ca-871a-11eb-1ec0-6323a2d2e2c1
let p = viz(df[:, :Weight])
	xlabel!("human weight")
	savefig("human-weight.svg")
	p
end

# ╔═╡ f8da9fb6-873e-11eb-233a-6de4effe46c9
let p = viz(df[:, :Height])
	xlabel!("human height")
	savefig("human-height.svg")
	p
end

# ╔═╡ Cell order:
# ╠═3221b210-8716-11eb-08d0-d9254fad7c98
# ╠═3e354594-8716-11eb-1e34-1f733bf7932d
# ╠═11921312-8718-11eb-0c6f-c3c312d8c9f0
# ╠═39cec4fe-871a-11eb-05e6-89d835d83388
# ╠═b0cc2484-871a-11eb-3f4b-3752b8a7776c
# ╠═bd9fd2ca-871a-11eb-1ec0-6323a2d2e2c1
# ╠═f8da9fb6-873e-11eb-233a-6de4effe46c9
