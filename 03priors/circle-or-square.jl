### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# ╔═╡ 5efd96d2-9553-11eb-1862-a5041e2664a4
begin
	using Flux, Plots, Distributions, Random
	using Turing
	using PlutoUI: with_terminal
end

# ╔═╡ 4c2bf89a-8c06-4c92-8ce8-3edf28e4f03d
md"# Circle or square?

We want to distinguish between squares and circles given a few noisy points from either. 
"

# ╔═╡ b3b1e907-f672-4ab6-81ac-9763b52c5db8
md"First, we need to get the data. Here is a simulator which draws either a noisy square or a noisy circle, with a given probability.
"

# ╔═╡ 8f263700-9554-11eb-13a9-5f85748cea5f
function simulate(n; pSquare=0.5, σ=0.025)
	r = rand(Gamma(10, 0.1))
	pt = Array{Float64,2}(undef, n, 2)
	α = rand(Uniform(0, 1), n)
	isSquare = rand(Bernoulli(pSquare))
	a = r .+ rand(Normal(0, σ), n)
	if isSquare
		b = r.*(-1 .+ 2 .* α) .+ rand(Normal(0, σ), n)
		for i in 1:n
			side = rand(Categorical(4))
			if side == 1
				pt[i, 1] = a[i]
				pt[i, 2] = b[i]
			elseif side == 2
				pt[i, 1] = -b[i]
				pt[i, 2] = a[i]
			elseif side == 3
				pt[i, 1] = -a[i]
				pt[i, 2] = -b[i]
			elseif side == 4
				pt[i, 1] = b[i]
				pt[i, 2] = -a[i]
			end
		end
	else
		φ = 2*π.*α
		pt[:, 1] = a.*cos.(φ)
		pt[:, 2] = a.*sin.(φ)
	end
	pt, isSquare
end

# ╔═╡ 64a8d690-6735-4d15-9c51-707fdf51014b
md"We can visualize the generated shapes. The more points are generated, the easier it is to recognize the shape. We choose the number of points where the shape is already recognizable by a human eye, but is still vague.
"

# ╔═╡ d0b024d9-d219-43e3-9300-f17aa22e5733
N = 25;

# ╔═╡ 07e161d8-955a-11eb-0e55-b51da968eefa
let
	pt, isSquare = simulate(N)
	Plots.scatter(pt[:, 1], pt[:, 2], 
		color=isSquare ? "red" : "blue", 
		xlims=(-2, 2), 
		ylims=(-2, 2), 
		size=(300, 300),
		label=missing)
end

# ╔═╡ 3e590eef-5265-491c-9913-bb3e4071cc5a
md"## Deep learning

Our first approach to this problem uses neural networks. We define a rather simple three layer fully connected network. This should be enough to fit a couple thousands of different shapes.
"

# ╔═╡ 90b1abb0-955c-11eb-3a72-69fcd0300219
function make_model(n)
	model = Chain(
		Dense(2*n, 20*n, tanh),
		Dense(20*n, 20*n, tanh),
		Dense(20*n, 2), 
		softmax)
end

# ╔═╡ 374cff25-7cb7-41df-b0aa-2f6725dcb23d
md"We won't always have the data to train the network, but for this study we assume that we do. We generated enough labeled samples — the training data — to train the model.
"

# ╔═╡ 96d8bbfc-955f-11eb-0e03-1d2c62ac771c
begin
	M = 1000
	data = Array{Float64, 2}(undef, N*2, M)
	labels = Array{Float64}(undef, M)
	for  i = 1:M
		pt, isSquare = simulate(N)
		data[:, i] = reshape(pt, :, 1)	
		labels[i] = isSquare
	end
end

# ╔═╡ 4275a459-f0f9-4cf6-a6ad-b06f5d760869
md"When presented a shape, the network returns whether it is a square or a circle. We compute the prediction accuracy as normalized Jackard distance — fraction of predictions where the network's prediction did not match the label. 

Note that even if the network does not agree with the label, that does not necessary mean a 'mistake': one can sample points from a square such that they can also come from a circle, especially a noisy one.
"

# ╔═╡ 297f1503-8f71-4c64-b2af-5815fd194396
function accuracy(model, x, y) 
	mean(abs.(Flux.onecold(y) - Flux.onecold(model(x))))
end

# ╔═╡ 6fb702e8-38c9-4558-a29c-bef9b1e8f4a9
md"### Randomly initialized model

First, let's compute the accuracy before training. It should be close to 0.5 — our predictions are random guesses, so they will be right half of the time.
"

# ╔═╡ 8b2d965e-955e-11eb-2827-f79fa210728a
begin 
	x = data
	y = Flux.onehotbatch(labels, 0:1)
	model = make_model(N)
	accuracy(model, x, y)
end

# ╔═╡ 6c21d870-cd66-41e1-b91c-1abc0e114175
md"### Trained model

We train the model to miminize the discrepancy between predictions and lables on the training data. Even with our simple neural architecture, we can achieve a remarkable accuracy.
"

# ╔═╡ 53c77b62-9561-11eb-30c8-972c6bcd5be0
begin 
	ps = Flux.params(model)
	opt = ADAM()
	batches = Flux.Data.DataLoader((x, y), batchsize=16)
	Epochs = 100
	for epoch in 1:Epochs
		for (xb, yb) in batches
			gs = gradient(ps) do 
				Flux.Losses.logitcrossentropy(model(xb), yb)
			end
			Flux.Optimise.update!(opt, ps, gs)
		end
		
	end
	accuracy(model, x, y)
end

# ╔═╡ 51d1c5c9-d3f2-4a57-9d6e-595addc32105
let i = rand(1:M)
	pt = reshape(data[:, i], :, 2)
	isSquare = labels[i]
	Plots.scatter(pt[:, 1], pt[:, 2])
	y = model(reshape(pt, :, 1))
	Plots.scatter(pt[:, 1], pt[:, 2], label=missing, 
		          color=isSquare>0 ? "red" : "blue"),
	Plots.histogram([0, 1], weights=y, bins=3, label=missing, xlabel="pSquare")
end
	

# ╔═╡ 325441ad-11b7-424d-93e6-7e1f4477517c
md"However this spectacular accuracy on training data does not translate to good performance on new shapes,  simulated by the same procedure. The predictions are only slightly better than random. 

The neural model didn't really learn to distinguish between circles and square. And we have little means to find out what actually it **did** learn to recognize.
"

# ╔═╡ 1de9fb5e-9562-11eb-3f0a-294a4deadaea
let (pt, isSquare) = simulate(N)
	y = model(reshape(pt, :, 1))
	Plots.scatter(pt[:, 1], pt[:, 2], label=missing, color=isSquare ? "red" : "blue"),
	Plots.histogram([0, 1], weights=y, bins=3, xlabel="pSquare", label=missing)
end

# ╔═╡ 7dfea9a5-2880-49ed-abe7-fa264794087a
md"Some call it **overfit**. But there is more than just overfit here. Our model did not encode our general human knowledge about circles and squares. It just selected a likely hypothesis which suits the training data. 
"

# ╔═╡ 6ec03277-cde3-46ee-ad2e-0cda0a70ef37
md"## Probabilistic programming

Instead of training a black-box _discriminative_ model, we can write a generative model which reflects our hypothesis that we are looking at either a centered square or a centered circle. 

It is simple to sample noisly from a circle. Square is a little more challenging, but we can approximate a square by four two-dimensional normal distributions:
"

# ╔═╡ ef244bbc-879d-4737-a565-9ff8295713c4
let r=rand(Gamma(2, 0.5)), σ = 0.01r, τ=0.5r, 
	square = rand(MixtureModel([
		MvNormal([r, 0], [σ, τ]),
		MvNormal([0, r], [τ, σ]),
		MvNormal([-r, 0], [σ, τ]),
		MvNormal([0, -r], [τ, σ])]),
		10000)
	Plots.scatter(square[1, :], square[2, :], label=missing)
end

# ╔═╡ ed870328-96fc-4339-bb7e-2aee47c7457c
md"### Generative model

Based on this, we can write a generative model. Note that our generative model draws either shapes resembling squares or shapes resembling circles, but does it in a different way than the simulator, and with a different distribution of sizes. We do not have to mimick the data perfectly --- what we need to specify is a reasonable set of hypotheses. _Probabilistic programming_ helps us accomplish this.
"

# ╔═╡ 809f68c8-9568-11eb-097a-510a04943f79
@model function ros(pt, ρ; pSquare = 0.5, σ=0.01) 
	N = length(pt[:,1])
	r ~ Gamma(4, 0.25)
	isSquare ~ Bernoulli(pSquare)
	if isSquare
		for i in 1:N
			pt[i, :] ~ MixtureModel([
					MvNormal([r, 0], [σ*r, r]),
					MvNormal([0, r], [r, σ*r]),
					MvNormal([-r, 0], [σ*r, r]),
					MvNormal([0, -r], [r, σ*r])])	
		end
	else
		for i in 1:N
			ρ[i] ~ Normal(r, 1.5σ)
		end
	end
end

# ╔═╡ 3a16b291-5926-4dd5-9502-e3cfd048ab3a
md"Our simple model does not need any training data to distringuish circles and squares with high confidence. However, training data can be used with probabilistic programs when desirable. Such models are caller _hierarchical_.
"

# ╔═╡ bf67b40e-956b-11eb-2f38-23d0a52d25f1
let (pt, isSquare) = simulate(N)
	ρ = sqrt.(pt[:, 1].^2 .+ pt[:, 2].^2)
	chain = sample(ros(pt, sqrt.(pt[:, 1].^2 .+ pt[:, 2].^2)), MH(), 1000)
	Plots.scatter(pt[:, 1], pt[:, 2], color=isSquare ? "red" : "blue"), 
	histogram(chain[:isSquare][:, 1], bins=2, xlims=(0, 1.5), 
		      xlabel="pSquare", label=missing),
	histogram(chain[:r][:, 1], xlabel="r", label=missing)
end

# ╔═╡ Cell order:
# ╟─4c2bf89a-8c06-4c92-8ce8-3edf28e4f03d
# ╠═5efd96d2-9553-11eb-1862-a5041e2664a4
# ╟─b3b1e907-f672-4ab6-81ac-9763b52c5db8
# ╠═8f263700-9554-11eb-13a9-5f85748cea5f
# ╟─64a8d690-6735-4d15-9c51-707fdf51014b
# ╠═d0b024d9-d219-43e3-9300-f17aa22e5733
# ╠═07e161d8-955a-11eb-0e55-b51da968eefa
# ╟─3e590eef-5265-491c-9913-bb3e4071cc5a
# ╠═90b1abb0-955c-11eb-3a72-69fcd0300219
# ╟─374cff25-7cb7-41df-b0aa-2f6725dcb23d
# ╠═96d8bbfc-955f-11eb-0e03-1d2c62ac771c
# ╟─4275a459-f0f9-4cf6-a6ad-b06f5d760869
# ╠═297f1503-8f71-4c64-b2af-5815fd194396
# ╟─6fb702e8-38c9-4558-a29c-bef9b1e8f4a9
# ╠═8b2d965e-955e-11eb-2827-f79fa210728a
# ╟─6c21d870-cd66-41e1-b91c-1abc0e114175
# ╠═53c77b62-9561-11eb-30c8-972c6bcd5be0
# ╠═51d1c5c9-d3f2-4a57-9d6e-595addc32105
# ╟─325441ad-11b7-424d-93e6-7e1f4477517c
# ╠═1de9fb5e-9562-11eb-3f0a-294a4deadaea
# ╟─7dfea9a5-2880-49ed-abe7-fa264794087a
# ╟─6ec03277-cde3-46ee-ad2e-0cda0a70ef37
# ╠═ef244bbc-879d-4737-a565-9ff8295713c4
# ╟─ed870328-96fc-4339-bb7e-2aee47c7457c
# ╠═809f68c8-9568-11eb-097a-510a04943f79
# ╟─3a16b291-5926-4dd5-9502-e3cfd048ab3a
# ╠═bf67b40e-956b-11eb-2f38-23d0a52d25f1
