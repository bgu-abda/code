### A Pluto.jl notebook ###
# v0.14.3

using Markdown
using InteractiveUtils

# ╔═╡ 88e679a8-a27a-11eb-3520-438d06ddb3eb
using Turing, StatsPlots, Distributions

# ╔═╡ 9bc60181-b14d-4a83-a223-e20a92ca86a7
md"# Marbles in a box

_From flat to hierarchical_

There is a box with four marbles, blue or white. We remove a marble from the box, record the color, and put the marble back. Given a series of observed marbles, what can we say about the box contents?
"

# ╔═╡ 889d3606-61cc-448c-80c9-cd53af7adcb6
begin
	nmarbles = 4
	observations = [0, 1, 1]
end

# ╔═╡ bd772d2b-d284-43e4-b985-739bb6cacc75
md"## Flat model with categorical prior

One simple approach is, following the example in class, impose a prior on all different combinations of the marbles in the box. We can precompute the probabilities 'counting' the number of ways each combination can be obtained.
"

# ╔═╡ d4946781-e0f8-44e5-8115-1929e2f15af6
begin
	nblues = collect(0:nmarbles)
	pnblues = zeros(length(nblues))
	for n in 1:length(nblues)
		pnblues[n] = binomial(length(nblues)-1, n-1)
	end
	pnblues	./= sum(pnblues)	 
end

# ╔═╡ 2c72b4b0-f0b9-4894-9cff-befe98109d05
@model function marbles1(obs)
	nblue ~ DiscreteNonParametric(nblues, pnblues)
	pblue = nblue/nmarbles
	for i in eachindex(obs)
		obs[i] ~ Bernoulli(pblue)
	end
end

# ╔═╡ 4283cfe1-4794-4b5b-a394-46d629cf2225
chn1 = sample(marbles1(observations), MH(), 10000);

# ╔═╡ c752d366-cdc8-4944-ace5-f00fd4c0f9f2
plot(chn1)

# ╔═╡ 2e49ce8e-e1c8-444a-9e43-c2da37991d95
md"## Flat model with binomial prior

A nicer solution is to observe that the number of blue marbles follows the Binomial distribution, and use it as the prior.
"

# ╔═╡ 3119403e-91f0-4877-a179-33a504f62b13
@model function marbles2(obs; pblue0 = 0.67)
	nblue ~ Binomial(nmarbles, pblue0)
	pblue = nblue/nmarbles
	for i in eachindex(obs)
		obs[i] ~ Bernoulli(pblue)
	end
end

# ╔═╡ 5a7386d1-b802-4bf9-9cb9-327eb3441b45
chn2 = sample(marbles2(observations), MH(), 100000);

# ╔═╡ 880e5860-96da-4ddb-95a0-e922b1d28578
plot(chn2)

# ╔═╡ bf2093ea-67f6-4c72-96c2-7b8558552831
md"## Multilevel with unknown factory probability

What if we do not know the probability of a blue marble in the bag of marbles? We can put a prior on that probability, and then use that (hyper)prior to define the prior on the distribution of the number of marbles.
"

# ╔═╡ 376c467c-f822-429c-81d4-28a9bd0212a1
@model function marbles3(obs; alpha=1, beta=1)
	pblue0 ~ Beta(alpha, beta)
	nblue ~ Binomial(nmarbles, pblue0)
	pblue = nblue/nmarbles
	for i in eachindex(obs)
		obs[i] ~ Bernoulli(pblue)
	end
end

# ╔═╡ 3588f833-e855-466e-8829-a0a501aacbe3
chn3 = sample(marbles3(observations), MH(), 10000);

# ╔═╡ 91975e71-e06e-495f-9c7c-8f4784a6f3b5
plot(chn3)

# ╔═╡ 0ec549ec-0318-4046-b43a-6704f8ea92d3
md"## Hierarchical

Imagine now that instead of a single box, we are given a number of boxes. We draw marbles a few times from _each_ box, and want to infer the probability of blue marbles in the bag from which the boxes are filled, as well as the distribution of blue marbles in each of the boxes.
"

# ╔═╡ ffe3e82e-a2f4-4131-9a5a-b07d63f80193
boxes = [[1, 1, 0], [0, 0, 1], [0, 1, 1, 1], [0, 0], [1, 1]];

# ╔═╡ f49fd1a1-2c03-4b2f-ab8a-87fd41bcaa79
@model function marblesh(boxes; alpha=1, beta=1)
	pblue0 ~ Beta(alpha, beta)
	nblue = TArray{Int}(length(boxes))
	pblue = TArray{Float64}(length(nblue))
	for ig in eachindex(boxes)
		nblue[ig] ~ Binomial(nmarbles, pblue0)
		pblue[ig] = nblue[ig]/nmarbles	
		for i in eachindex(boxes[ig])
			boxes[ig][i] ~ Bernoulli(pblue[ig])
		end
	end
end

# ╔═╡ dcaf83c6-90e3-40d7-8db1-156a1daed413
chnh = sample(marblesh(boxes), MH(),  10000);

# ╔═╡ 2c42bf75-0c37-4bee-9c87-f92ceb2255d6
plot(chnh)

# ╔═╡ 5c51305c-a4dc-45b8-b6e2-04a585812f8d
let c  = [9, 8*2, 3*4]
	c / sum(c)
end

# ╔═╡ 6c084ff4-09bb-4f63-a6ea-1e1b3ff4d918
binomial(4, 1)

# ╔═╡ Cell order:
# ╠═88e679a8-a27a-11eb-3520-438d06ddb3eb
# ╟─9bc60181-b14d-4a83-a223-e20a92ca86a7
# ╠═889d3606-61cc-448c-80c9-cd53af7adcb6
# ╟─bd772d2b-d284-43e4-b985-739bb6cacc75
# ╠═d4946781-e0f8-44e5-8115-1929e2f15af6
# ╠═2c72b4b0-f0b9-4894-9cff-befe98109d05
# ╠═4283cfe1-4794-4b5b-a394-46d629cf2225
# ╠═c752d366-cdc8-4944-ace5-f00fd4c0f9f2
# ╟─2e49ce8e-e1c8-444a-9e43-c2da37991d95
# ╠═3119403e-91f0-4877-a179-33a504f62b13
# ╠═5a7386d1-b802-4bf9-9cb9-327eb3441b45
# ╠═880e5860-96da-4ddb-95a0-e922b1d28578
# ╟─bf2093ea-67f6-4c72-96c2-7b8558552831
# ╠═376c467c-f822-429c-81d4-28a9bd0212a1
# ╠═3588f833-e855-466e-8829-a0a501aacbe3
# ╠═91975e71-e06e-495f-9c7c-8f4784a6f3b5
# ╟─0ec549ec-0318-4046-b43a-6704f8ea92d3
# ╠═ffe3e82e-a2f4-4131-9a5a-b07d63f80193
# ╠═f49fd1a1-2c03-4b2f-ab8a-87fd41bcaa79
# ╠═dcaf83c6-90e3-40d7-8db1-156a1daed413
# ╠═2c42bf75-0c37-4bee-9c87-f92ceb2255d6
# ╠═5c51305c-a4dc-45b8-b6e2-04a585812f8d
# ╠═6c084ff4-09bb-4f63-a6ea-1e1b3ff4d918
