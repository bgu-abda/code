### A Pluto.jl notebook ###
# v0.14.3

using Markdown
using InteractiveUtils

# ╔═╡ bce31d6a-a28e-11eb-02d5-796124bef7d7
using Turing, DataFrames, CSV, StatsPlots, Distributions

# ╔═╡ 0dd28fdc-3b7d-4d8e-9296-4038bed02525
md"# Reed frogs survival

Example from Ch. 13 of Statistical rethinking
"

# ╔═╡ 376d1b3d-b690-48ed-9a60-62f1e4d9472c
begin
	df = CSV.File("reedfrogs.csv") |> DataFrame
	density = df[!, :density]
	survival = df[!, :surv]
	df
end

# ╔═╡ 9914aba0-93cf-435c-9226-d3ad74153715
logistic(α) = 1/(1 + exp(-α))

# ╔═╡ 9aa92a5b-1a47-438e-a05f-267d4d868a4b
let 
	x = -5:0.01:5
	y = logistic.(x)

	plot(x, y)
end

# ╔═╡ 730e96dc-4ddd-4fc9-9493-4071870d3ac7
md"## Pooled model"

# ╔═╡ 8e63a9a8-33e0-44b9-8f1c-c4674fb1c844
@model function reedfrogs_pooled(density, survival)
	α ~ Normal(0, 1.5)
	# p = logistic(α)
	for i in eachindex(density)
		# survival[i] ~ Binomial(density[i], p)
		survival[i] ~ BinomialLogit(density[i], α)
	end
end

# ╔═╡ c3d2a4d5-daa7-4b74-b0bc-44000fcabd20
chn_pooled = sample(reedfrogs_pooled(density, survival), HMC(0.1, 5), 1000);

# ╔═╡ 82825cc3-e625-4b44-9e41-bfd3fdf12fbb
StatsPlots.density(logistic.(chn_pooled[:α]), xlabel="α", label=missing)

# ╔═╡ 1497b5f4-c235-44cf-bf53-fa58089c5d06
histogram(df[!, :propsurv], bins=100)

# ╔═╡ 59ffc4d3-da2b-4a00-bbf0-bba6072ef23c
md"## Separate model"

# ╔═╡ 8a7f4658-335f-498f-8b01-6243fdee0394
@model function reedfrogs_separate(density, survival) 
	α ~ MvNormal(fill(0, length(density)), 1.5)
	for i in eachindex(density)
		survival[i] ~ BinomialLogit(density[i], α[i])
	end
end

# ╔═╡ 110c342c-110f-45ee-8fd9-a1d2c06c8815
chn_separate = sample(reedfrogs_separate(density, survival), HMC(0.1, 5), 10000);

# ╔═╡ 6a727ada-2799-456f-9d19-7f56fcf08a78
let 
	raw = survival ./ density
	inferred = logistic.(reshape(mean(Array(group(chn_separate, :α)), dims=1), :))
	scatter(density, raw, inferred, 
		    ylim=(0, 1), zlim=(0, 1),
		    xlabel="density", ylabel="raw", zlabel="inferred", label=missing)
end

# ╔═╡ 5a96ac32-98ec-47a3-9032-be888d9c8efe
md"## Hierarchical model"

# ╔═╡ 609013e0-be38-4f3c-a1f5-1cc6f159ee26
@model function reedfrogs(density, survival) 
	σ ~ Exponential(1)
	α0 ~ Normal(0, 1.5)
	α ~ MvNormal(fill(α0, length(density)), σ)
	for i in eachindex(density)
		survival[i] ~ BinomialLogit(density[i], α[i])
	end
end

# ╔═╡ 988bddcc-ebe9-4527-aa5a-aaffe5c459e4
chn = sample(reedfrogs(density, survival), HMC(0.1, 5), 1000);

# ╔═╡ c5a44f5b-8aff-45fb-b1ed-12bd8e7102e9
let 
	raw = survival ./ density
	inferred = logistic.(reshape(mean(Array(group(chn, :α)), dims=1), :))
	scatter(density, raw, inferred, 
		    ylim=(0, 1), zlim=(0, 1),
		    xlabel="density", ylabel="raw", zlabel="inferred", label=missing)
end

# ╔═╡ 96caa86a-c32d-459c-93c0-418df7212420
histogram(logistic.(chn[:α0]), xlabel="α0", label=missing)


# ╔═╡ 056c134f-184a-45df-a32c-8a40b050b0f1
histogram(chn[:σ], xlabel="σ", label=missing)


# ╔═╡ Cell order:
# ╠═0dd28fdc-3b7d-4d8e-9296-4038bed02525
# ╠═bce31d6a-a28e-11eb-02d5-796124bef7d7
# ╠═376d1b3d-b690-48ed-9a60-62f1e4d9472c
# ╠═9914aba0-93cf-435c-9226-d3ad74153715
# ╠═9aa92a5b-1a47-438e-a05f-267d4d868a4b
# ╟─730e96dc-4ddd-4fc9-9493-4071870d3ac7
# ╠═8e63a9a8-33e0-44b9-8f1c-c4674fb1c844
# ╠═c3d2a4d5-daa7-4b74-b0bc-44000fcabd20
# ╠═82825cc3-e625-4b44-9e41-bfd3fdf12fbb
# ╠═1497b5f4-c235-44cf-bf53-fa58089c5d06
# ╟─59ffc4d3-da2b-4a00-bbf0-bba6072ef23c
# ╠═8a7f4658-335f-498f-8b01-6243fdee0394
# ╠═110c342c-110f-45ee-8fd9-a1d2c06c8815
# ╠═6a727ada-2799-456f-9d19-7f56fcf08a78
# ╟─5a96ac32-98ec-47a3-9032-be888d9c8efe
# ╠═609013e0-be38-4f3c-a1f5-1cc6f159ee26
# ╠═988bddcc-ebe9-4527-aa5a-aaffe5c459e4
# ╠═c5a44f5b-8aff-45fb-b1ed-12bd8e7102e9
# ╠═96caa86a-c32d-459c-93c0-418df7212420
# ╠═056c134f-184a-45df-a32c-8a40b050b0f1
