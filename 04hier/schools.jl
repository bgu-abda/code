### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ 0addd4f0-a286-11eb-3298-dd8c0a3fb48b
using Turing, CSV, DataFrames, StatsPlots, Distributions

# ╔═╡ 73767c0b-4352-44b6-92f7-70c267c1247f
md"# 8 schools

The eight schools problem (Rubin 1981) considers the effectiveness of SAT coaching programs conducted in parallel at eight schools. It has become a classic problem (Bayesian Data Analysis, Stan) that illustrates the usefulness of hierarchical modeling for sharing information between exchangeable groups.
"

# ╔═╡ 80524eb0-302c-4168-9ed1-a1b1f3390657
begin
	df = CSV.File("schools.csv") |> DataFrame
	effects = Float64.(df[!, :effect])
	errors = Float64.(df[!, :error])
	df
end

# ╔═╡ 554c9835-4c13-4d67-9b28-d49eaeb85381
@model function schools(effects, errors) 
	μ ~ Normal(0, 100)
	τ ~ LogNormal(0, 10)
	η ~ MvNormal(zeros(length(effects)), 1)
	θ = μ .+ τ .* η
	effects ~ MvNormal(θ, errors)
end

# ╔═╡ 8ddfbeef-b9e3-4aab-8b1e-3012a85c2546
chn = sample(schools(effects, errors), HMC(0.2, 10), 1000);

# ╔═╡ 430eeae3-f36a-4cdd-a22d-f8ac99c38937
plot(chn)

# ╔═╡ 2609bea7-9f4b-4fb0-8830-005561222459
 mean(chn[:μ]) .+ mean(chn[:τ]) .* mean(Array(group(chn, :η)))

# ╔═╡ 1b9807cb-7197-4f31-a073-471234365eb3
dfmean = mean(group(chn, :η))

# ╔═╡ 425b0d46-491c-4db4-9f52-29c23879d5d3
md"## Estimated effects"

# ╔═╡ 35f47898-90fd-4299-be52-72fd42b2045f
begin
	var = 100
	estimated_effects = mean(chn[:μ]) .+ var .* mean(Array(group(chn, :η)), dims=1)
end

# ╔═╡ Cell order:
# ╟─73767c0b-4352-44b6-92f7-70c267c1247f
# ╠═0addd4f0-a286-11eb-3298-dd8c0a3fb48b
# ╠═80524eb0-302c-4168-9ed1-a1b1f3390657
# ╠═554c9835-4c13-4d67-9b28-d49eaeb85381
# ╠═8ddfbeef-b9e3-4aab-8b1e-3012a85c2546
# ╠═430eeae3-f36a-4cdd-a22d-f8ac99c38937
# ╠═2609bea7-9f4b-4fb0-8830-005561222459
# ╠═1b9807cb-7197-4f31-a073-471234365eb3
# ╟─425b0d46-491c-4db4-9f52-29c23879d5d3
# ╠═35f47898-90fd-4299-be52-72fd42b2045f
