### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ 88e679a8-a27a-11eb-3520-438d06ddb3eb
using Turing, StatsPlots, Distributions

# ╔═╡ 9bc60181-b14d-4a83-a223-e20a92ca86a7
md"# Marbles in boxes

_Representing and analysing posterior distributions_

We are given a number of boxes. We draw marbles a few times from each box, and want to infer the probability of blue marbles in the bag from which the boxes are filled, as well as the distribution of blue marbles in each of the boxes.
"

# ╔═╡ 889d3606-61cc-448c-80c9-cd53af7adcb6
begin
	blue = 1
	white = 0
	nmarbles = 4
	boxes = [[blue, blue, white], 
		     [white, white, blue], 
		     [white, blue, blue, blue],
		     [white, white], 
		     [blue, blue, blue],
			 [blue]]
end;

# ╔═╡ f49fd1a1-2c03-4b2f-ab8a-87fd41bcaa79
@model function marbles(boxes; alpha=1, beta=1)
	pblue0 ~ Beta(alpha, beta)
	nblue = TArray{Int}(length(boxes))
	pblue = TArray{Float64}(length(nblue))
	for ig in eachindex(boxes)
		nblue[ig] ~ Binomial(nmarbles, pblue0)
		pblue[ig] = nblue[ig]/nmarbles	
		for i in eachindex(boxes[ig])
			boxes[ig][i] ~ Bernoulli(pblue[ig])
		end
	end
end

# ╔═╡ dcaf83c6-90e3-40d7-8db1-156a1daed413
chn = sample(marbles(boxes), Gibbs(HMC(0.1, 5, :pblue0), MH(:nblue)),  100000);

# ╔═╡ 2c42bf75-0c37-4bee-9c87-f92ceb2255d6
plot(chn)

# ╔═╡ 284ab408-1c48-464a-8993-f8c1e089fedb
function simbox(chn, ibox, ndraws, size=10000)
	draws = zeros(size)
	nblues = sample(chn[Symbol("nblue[$ibox]")], size)
	for i in 1:size
		draws[i] = rand(Binomial(ndraws, nblues[i]/nmarbles))
	end
	draws
end

# ╔═╡ e656f901-eddc-47a0-a378-5a9c74c19f36
let 
	p = plot(layout=length(boxes))
	for i in 1:length(boxes)
		histogram!(simbox(chn, i, 3), normalize=:probability, subplot=i, label=missing, xlabel="box $i: $(boxes[i])")
	end
	p
end

# ╔═╡ b6c927bb-a099-4116-9cf0-55f3d0f4f1a6
function simnewbox(chn, ndraws, size=10000)
		draws = zeros(size)
	pblue0 = sample(chn[:pblue0], size)
	for i in 1:size
		nblue = rand(Binomial(nmarbles, pblue0[i]))
		draws[i] = rand(Binomial(ndraws, nblue/nmarbles))
	end
	draws
end

# ╔═╡ 265f669d-34e2-4ed0-8c7c-e309bc380bf3
histogram(simnewbox(chn, 3), normalize=:probability, label=missing)

# ╔═╡ Cell order:
# ╟─88e679a8-a27a-11eb-3520-438d06ddb3eb
# ╟─9bc60181-b14d-4a83-a223-e20a92ca86a7
# ╠═889d3606-61cc-448c-80c9-cd53af7adcb6
# ╠═f49fd1a1-2c03-4b2f-ab8a-87fd41bcaa79
# ╠═dcaf83c6-90e3-40d7-8db1-156a1daed413
# ╠═2c42bf75-0c37-4bee-9c87-f92ceb2255d6
# ╠═284ab408-1c48-464a-8993-f8c1e089fedb
# ╠═e656f901-eddc-47a0-a378-5a9c74c19f36
# ╠═b6c927bb-a099-4116-9cf0-55f3d0f4f1a6
# ╠═265f669d-34e2-4ed0-8c7c-e309bc380bf3
