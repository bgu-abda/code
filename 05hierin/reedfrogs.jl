### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ bce31d6a-a28e-11eb-02d5-796124bef7d7
using Turing, DataFrames, CSV, StatsPlots, Distributions

# ╔═╡ 0dd28fdc-3b7d-4d8e-9296-4038bed02525
md"# Reed frogs survival

Example from Ch. 13 of Statistical rethinking.
"

# ╔═╡ b33bb40c-436e-4e94-941e-683b2de2aedc
md"## Data
"

# ╔═╡ 376d1b3d-b690-48ed-9a60-62f1e4d9472c
begin
	df = CSV.File("reedfrogs.csv") |> DataFrame
	density = df[!, :density]
	survival = df[!, :surv]
	predator = df[!, :pred]
	df
end

# ╔═╡ 9914aba0-93cf-435c-9226-d3ad74153715
logistic(α) = 1/(1 + exp(-α))

# ╔═╡ 9537de5d-7ec1-4ed9-a413-400678edea03
md"## Model

Unlike in the earlier version of the study, we now distinguish between tanks with and without predation.
"

# ╔═╡ 609013e0-be38-4f3c-a1f5-1cc6f159ee26
@model function reedfrogs(density, survival, predator) 
	σ ~ Exponential(1)
	α0 ~ Normal(0, 1.5)
	αp ~ Normal(0, 1.5)
	αt ~ MvNormal(fill(α0, length(density)), σ)
	for i in eachindex(density)
		α = αt[i]
		if predator[i] == "pred"
			α += αp
		end
		survival[i] ~ BinomialLogit(density[i], α)
	end
end

# ╔═╡ e8c88b61-d455-4713-91fe-8edfe79748e9
md"## Inference"

# ╔═╡ 988bddcc-ebe9-4527-aa5a-aaffe5c459e4
chn = sample(reedfrogs(density, survival, predator), NUTS(), MCMCThreads(), 1000, 4);

# ╔═╡ 95e420be-0f18-4f0a-8a9b-c503f360ceca
md"## Posterior

We can now plot the posterior for each tadpole density, comparing to the separate estimates. If the hierarchical estimates were the same as the separate ones, the dots would lie on the green diagonals. Deviation from the green lines occurs because of shrinkage.

The deviation is more prominent in smaller tanks. This is because the more data is available the stronger is the influence of the data on the posterior.
"

# ╔═╡ af9ff1d2-f0bd-4d2a-9fb5-5a220f554e0d
let 
	p = plot(layout=(1, 3), size=(1000, 300))
	raw = survival ./ density
	αt = reshape(mean(Array(group(chn, :αt)), dims=1), :)
	αp = mean(chn, :αp) .* (predator .== "pred")
	inferred = logistic.(αt + αp)
	for (i, d) in enumerate(unique(density))
		indices = density .== d
		scatter!(raw[indices], inferred[indices], 
				xlim=(-0.1, 1.1), ylim=(-0.1, 1.1),
				xlabel="raw", ylabel="inferred", title="$d tadpoles",
				label=missing, subplot=i)
		plot!([0, 1], [0, 1], subplot=i, line=(1, :dash, :green))
	end
	p
end

# ╔═╡ 1c50cabf-16c6-4f07-9111-dedabd28d5ae
md"Another way to look at the posterior is through histograms of marginal distributions of α0, σ, and αp. The mean of αp is significantly negative, and α0 and αp approximately cancel out. That can be interpreted as average survival probability of about 0.5 in tanks with predation.
"

# ╔═╡ 96caa86a-c32d-459c-93c0-418df7212420
let
	plot(layout=3)
	histogram!(chn[:α0], xlabel="α0", label=missing,  subplot=1)
	histogram!(chn[:σ], xlabel="σ", label=missing, subplot=2)
	histogram!(chn[:αp], xlabel="αp", label=missing, subplot=3)
end

# ╔═╡ Cell order:
# ╟─0dd28fdc-3b7d-4d8e-9296-4038bed02525
# ╠═bce31d6a-a28e-11eb-02d5-796124bef7d7
# ╟─b33bb40c-436e-4e94-941e-683b2de2aedc
# ╠═376d1b3d-b690-48ed-9a60-62f1e4d9472c
# ╠═9914aba0-93cf-435c-9226-d3ad74153715
# ╟─9537de5d-7ec1-4ed9-a413-400678edea03
# ╠═609013e0-be38-4f3c-a1f5-1cc6f159ee26
# ╟─e8c88b61-d455-4713-91fe-8edfe79748e9
# ╠═988bddcc-ebe9-4527-aa5a-aaffe5c459e4
# ╟─95e420be-0f18-4f0a-8a9b-c503f360ceca
# ╠═af9ff1d2-f0bd-4d2a-9fb5-5a220f554e0d
# ╟─1c50cabf-16c6-4f07-9111-dedabd28d5ae
# ╠═96caa86a-c32d-459c-93c0-418df7212420
