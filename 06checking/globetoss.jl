### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 9c2725e8-ad9d-11eb-2c42-13b0c0814258
using Turing, Distributions, Statistics, StatsPlots, Random

# ╔═╡ 162dcc45-359b-49c7-b667-345812913670
begin
	L = 0
	W = 1
	data = [L, L, W, W, W, W, W, L, L, L, L, L, W, W, W, W, W, W, W, W]
	# data = [i % 2 for i in 1:20]
	# data = rand(0:1, 20)
end

# ╔═╡ e847daf0-746e-43aa-bfbd-dc157c9629d0
@model function globetoss(y::Array{Int,1})
	p ~ Beta(1, 1)
	for i = eachindex(y)
		y[i] ~ Bernoulli(p)
	end
end

# ╔═╡ 04b65ea9-2721-439e-9b4a-1283f31d3ff1
begin
	chain = sample(globetoss(data), MH(), 10000)
	histogram(chain[:p], xlabel="p", xlim=(0, 1), seriestype=:pdf, label=missing)
end

# ╔═╡ 5b4451c7-ecbc-4ea6-a064-0e3bcd4bde17
function simulate_globetoss(chain, size=1000)
	ps = sample(chain[:p], size)
	tosses = Array{Int, 2}(undef, size, length(data))
	for i in 1:size
		for j in 1:length(data)
			tosses[i, j] = rand(Bernoulli(ps[i]))
		end
	end
	tosses
end

# ╔═╡ b6554bc6-e972-430a-8e97-b743cf8e0e10
function count_switches(data)
	count = 0
	for i = 1:length(data)-1
		if data[i] != data[i+1]
			count += 1
		end
	end
	count
end

# ╔═╡ ad651c00-eacb-4393-8bd1-d23c983ea637
count_switches(simulate_globetoss(chain, 1))

# ╔═╡ db4dddfa-8e20-49fb-84f2-b29fd85ece8f
let N=100
	switches = Array{Int}(undef, N)
	simulations = simulate_globetoss(chain, N)
	for i in 1:N
		switches[i] = count_switches(simulations[i, :])
	end
	switch_data = count_switches(data)
	p_value = length(switches[switches .< switch_data])/ length(switches)
	histogram(switches, label="simulated", xlabel="p_value = $p_value")
	vline!([switch_data], lw=2, label="observed")
end

# ╔═╡ e21ca6af-0c84-4b14-92cf-89339d21d81f
md"## Accounting for dependencies"

# ╔═╡ 4508d1ac-7a69-4cbd-ac76-8d825477df50
@model function globetoss_walk(y::Array{Int,1})
	p ~ Beta(1, 1)
	q ~ Beta(1, 1)
	yprev ~ Bernoulli(p)
	for i = eachindex(y)
		if yprev == 1
			y[i] ~ Bernoulli(q+(1-q)*p)
		else
			y[i] ~ Bernoulli((1-q)*p)
		end
		yprev = y[i]
	end
end

# ╔═╡ 9c794942-3aeb-4c70-a7c5-22e334515d59
begin
	chain_walk = sample(globetoss_walk(data), MH(), 10000)
	plt = plot(layout=2)
	histogram!(plt, chain_walk[:p], xlabel="p", xlim=(0, 1), seriestype=:pdf, label=missing, subplot=1)
	histogram!(plt, chain_walk[:q], xlabel="q", xlim=(0, 1), seriestype=:pdf, label=missing, subplot=2)
end

# ╔═╡ Cell order:
# ╠═9c2725e8-ad9d-11eb-2c42-13b0c0814258
# ╠═162dcc45-359b-49c7-b667-345812913670
# ╠═e847daf0-746e-43aa-bfbd-dc157c9629d0
# ╠═04b65ea9-2721-439e-9b4a-1283f31d3ff1
# ╠═5b4451c7-ecbc-4ea6-a064-0e3bcd4bde17
# ╠═b6554bc6-e972-430a-8e97-b743cf8e0e10
# ╠═ad651c00-eacb-4393-8bd1-d23c983ea637
# ╠═db4dddfa-8e20-49fb-84f2-b29fd85ece8f
# ╟─e21ca6af-0c84-4b14-92cf-89339d21d81f
# ╠═4508d1ac-7a69-4cbd-ac76-8d825477df50
# ╠═9c794942-3aeb-4c70-a7c5-22e334515d59
