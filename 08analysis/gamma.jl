### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 0928bb8e-b878-11eb-1712-8d26337f9f6a
using CSV, DataFrames, Turing, MCMCChains, StatsPlots, Statistics

# ╔═╡ 8c77e5e6-9266-4a0c-8102-d0809094c0e5
md"# Model Analysis"

# ╔═╡ edc4c5a6-76f2-432b-8937-e92cba1e528a
md"An example of model checking and evaluation. 

## Problem

Fit a model to a single-dimensional data set. The data set is
generated from Gamma(2, 0.5), but we pretend that we do not know
that.

## Solution outline

We examine the normal and the log-normal model, both with two
unknown parameters.

* We fit each model to the data, and compare
  extreme values against posterior samples to check whether and
  when the model is appropriate for the data.
* Then, we evaluate the models using information criteria and
  cross-validation. 
* Finally, we expand the log-normal model by putting a hyperprior
  on the model's parameters and compare the expanded model with the
  orginal one.
"

# ╔═╡ 2de30420-1ead-42b3-9d1a-32c26fd33f33
md"## Models

Load and visualize the data:"

# ╔═╡ 5029c98d-823f-4619-aa14-0a16be8b63c7
begin
	data = let df = CSV.File("gamma.csv") |> DataFrame
		df[!, 1]
	end
	histogram(data, bins=10)
end

# ╔═╡ d308c693-1ac7-4a4d-9a46-a353c01e38af
data

# ╔═╡ b0077502-5299-4a45-8ac3-f251ecfc89d6
md"Define the models and run inference:"

# ╔═╡ 7ba722a1-2227-4b4a-9f25-a857db96791e
@model function NormalModel(data) 
	μ ~ Normal(0, 10)
	σ ~ Exponential(1)
	data ~ Normal(μ, σ)
end		

# ╔═╡ 4de08b00-f69b-4e60-bd8d-215cdb0cd705
chainNormal = sample(NormalModel(data), NUTS(), MCMCThreads(), 5000, 4);

# ╔═╡ 16bb1d0f-ff11-4b5b-b729-36d54953133b
plot(chainNormal)

# ╔═╡ 8996fcad-beb9-4398-a228-5ab7c7a8f7a7
@model function LogNormalModel(data) 
	μ ~ Normal(0, 10)
	σ ~ Exponential(1)
	data ~ LogNormal(μ, σ)
end		

# ╔═╡ 74ed2805-8319-4bc6-a49a-e57110359298
 chainLogNormal = sample(LogNormalModel(data), NUTS(),  MCMCThreads(), 5000, 4);

# ╔═╡ 0ab1c5e0-b813-44fe-be0b-5afae778ce06
plot(chainLogNormal)

# ╔═╡ 03f511e2-ec6f-4fc8-8b23-1e7b000e44b1
@model function HierLogNormalModel(data)
	η ~ Normal(0, 10)
	μ ~ Normal(η, 1)
	σ ~ Gamma(2, 0.5)
	data ~ LogNormal(μ, σ)
end

# ╔═╡ 9625e817-28a0-4e84-a5cd-ea0885f089a2
 chainHierLogNormal = sample(HierLogNormalModel(data), NUTS(),  MCMCThreads(), 5000, 4);

# ╔═╡ 0f10ba91-813d-437a-b758-1b53fb3289ab
plot(chainHierLogNormal)

# ╔═╡ 847a53cd-a680-4c5a-9a5f-9b4ad5f6a202
md"Note that with parameters and inference algorithms we've chosen, the chains mix well and the posteriors from all chains match. We can use formal convergence criteria, available from MCMCChains:
"

# ╔═╡ d7145ba9-9168-4716-91e2-9a268a752715
MCMCChains.ess(chainNormal)

# ╔═╡ 6b04d4d9-abb9-48f4-ba38-9fed330563e1
MCMCChains.ess(chainLogNormal)

# ╔═╡ 03b8a293-6ad3-4b2d-a0bb-5d896ba50cfc
MCMCChains.ess(chainHierLogNormal)

# ╔═╡ 0ec8e74c-75c0-44d4-b995-690382c58691
md"## Checking

Now that we have the posterior samples we can check the models. Since our inference will likely match summary statistics, we will instead check minimum, maximum, and median values.
"

# ╔═╡ 13a2ad81-a042-4269-b3d8-ede0638b4b3d
dataMin, dataMax, dataMed = minimum(data), maximum(data), median(data)

# ╔═╡ b2be21e4-3a35-4be9-af77-c118c2d675dd
function simulate(chain, dist, nobs=length(data))
	mus = chain[:μ][:, 1]
	sigmas = chain[:σ][:, 1]
	simulation = Vector{Float64}(undef, nobs)
	for iobs in 1:nobs
		i = rand(1:length(mus))
		μ = mus[i]
		σ = sigmas[i]
		simulation[iobs] = rand(dist(μ, σ))
	end
	simulation
end

# ╔═╡ 589ec279-3d50-4def-a00e-c01e0f817719
md"For illustration, we plot overlaid histograms of data and 3 simulations from each of the models. This informal check suggests quality of fit of each the models."

# ╔═╡ 23d16f89-500f-4213-aa50-fa7548004676
models = [
			("Normal", chainNormal, Normal),
		    ("LogNormal", chainLogNormal, LogNormal),
    		("HierLogNormal", chainHierLogNormal, LogNormal)
		];

# ╔═╡ 86b47439-ac69-4de1-92b2-a87ea4cbe616
let plt = plot(layout=(1, 3), size=(900, 300))
	for (i, (title, chain, dist)) in enumerate(models)
		plot!(title=title, subplot=i)
		histogram!(data, subplot=i, alpha=0.5, label="data", bins=25, xlim=(-2, 5))
		for j in 1:3
			histogram!(simulate(chain, dist),
				       subplot=i, alpha=0.3, label="simulation $j", 
				       bins=10)
		end
	end
	plt
end

# ╔═╡ 0da565ff-8cfb-44f0-b6dd-810521739a94
md"We will now generate 100 simulations from each model, and plot histograms of minimum, maximum, and median for each model, with marks for the values for observations for comparison (red vertical lines)."

# ╔═╡ 04d65293-e48d-426d-9cbb-d795d58abe5a
let NSim = 1000
	plt = plot(layout=(3, 3), size=(900, 900))
	plot!(xlabel="min", subplot=7)
	plot!(xlabel="max", subplot=8)
	plot!(xlabel="median", subplot=9)
	for (i, (title, chain, dist)) in enumerate(models)
		plot!(ylabel=title, subplot=3*(i-1)+1)
		sims = map(i -> simulate(chain, dist), 1:NSim)
		mins, maxs, meds = minimum.(sims), maximum.(sims), median.(sims)
		histogram!(mins, subplot=3*(i-1)+1, label=missing)
		histogram!(maxs, subplot=3*(i-1)+2, label=missing)
		histogram!(meds, subplot=3*(i-1)+3, label=missing)
		vline!(plt, [dataMin], subplot=3*(i-1)+1, label=missing, lw=4)
		vline!(plt, [dataMax], subplot=3*(i-1)+2, label=missing, lw=4)
		vline!(plt, [dataMed], subplot=3*(i-1)+3, label=missing, lw=4)
		plot!(plt, subplot=3*(i-1)+1, title="p-value=$(sum(mins .< dataMin)/NSim)")
		plot!(plt, subplot=3*(i-1)+2, title="p-value=$(sum(maxs .> dataMax)/NSim)")
		plot!(plt, subplot=3*(i-1)+3, title="p-value=$(sum(meds .> dataMed)/NSim)")
	end
	plt
end

# ╔═╡ cc38553e-dd22-4fee-a71e-e691529bcf7c
md"## Evaluation

We will use WAIC criterion to evaluate the models since samples are exchangeable."

# ╔═╡ 61330842-c78e-4208-baee-bc743505750b
function elpd(chain, dist, data)
	mus = chain[:μ][:, 1]
	sigmas = chain[:σ][:, 1]
	elpd = 0.
	for i in eachindex(data)
		for j in eachindex(mus)
			elpd += logpdf(dist(mus[j], sigmas[j]), data[i])
		end
	end
	elpd /= length(mus)
	elpd
end

# ╔═╡ b5a1601b-d48d-4d40-8a2b-ff3084730a4d
md"Let's first compare expected log predictive density:"

# ╔═╡ 1592bcaf-08db-4bd7-be18-96797703eeca
elpdNormal, elpdLogNormal, elpdHierLogNormal = elpd(chainNormal, Normal, data), elpd(chainLogNormal, LogNormal, data), elpd(chainHierLogNormal, LogNormal, data)

# ╔═╡ 4c6cfecb-be6a-4f27-bc20-83016dada5d3
md"LogHierModel has the highest log predictive density, but it may be due to a large number of parameters. Let's compute the penalty, according to WAIC, and compare again."

# ╔═╡ 27ae1e12-f33a-4442-b3cc-5605b6f65c33
function pwaic(chain, dist, data)
	mus = chain[:μ][:, 1]
	sigmas = chain[:σ][:, 1]
	p = 0.
	for i in eachindex(data)
		lpds = []
		for j in eachindex(mus)
			push!(lpds, logpdf(dist(mus[j], sigmas[j]), data[i]))
		end
		p+= var(lpds)
	end
	p
end

# ╔═╡ 5cc8d730-b737-48c4-a0f7-3af717c05a81
pNormal, pLogNormal, pHierLogNormal = pwaic(chainNormal, Normal, data), pwaic(chainLogNormal, LogNormal, data), pwaic(chainHierLogNormal, LogNormal, data)

# ╔═╡ 25bd604d-16cc-453f-99c5-c65e9036e442
md"We see that despite an extra parameter in the model, the penalty is roughly the same for the hierarchical model. Finally, we can compute WAIC for each of the models."

# ╔═╡ c0b545e3-eaac-44ff-8302-3d4f282302b3
-2*(elpdNormal - pNormal), -2*(elpdLogNormal - pLogNormal), -2*(elpdHierLogNormal - pHierLogNormal)

# ╔═╡ 6d4cbc71-95b8-4945-b887-5456d09dc05c
md"LogNormal model gets a lower WAIC than normal, and the ‘hierarchical’ model is not that different. We can safely go with the LogNormal model, based on checking and evaluation."

# ╔═╡ Cell order:
# ╟─8c77e5e6-9266-4a0c-8102-d0809094c0e5
# ╠═0928bb8e-b878-11eb-1712-8d26337f9f6a
# ╟─edc4c5a6-76f2-432b-8937-e92cba1e528a
# ╟─2de30420-1ead-42b3-9d1a-32c26fd33f33
# ╠═5029c98d-823f-4619-aa14-0a16be8b63c7
# ╠═d308c693-1ac7-4a4d-9a46-a353c01e38af
# ╟─b0077502-5299-4a45-8ac3-f251ecfc89d6
# ╠═7ba722a1-2227-4b4a-9f25-a857db96791e
# ╠═4de08b00-f69b-4e60-bd8d-215cdb0cd705
# ╠═16bb1d0f-ff11-4b5b-b729-36d54953133b
# ╠═8996fcad-beb9-4398-a228-5ab7c7a8f7a7
# ╠═74ed2805-8319-4bc6-a49a-e57110359298
# ╠═0ab1c5e0-b813-44fe-be0b-5afae778ce06
# ╠═03f511e2-ec6f-4fc8-8b23-1e7b000e44b1
# ╠═9625e817-28a0-4e84-a5cd-ea0885f089a2
# ╠═0f10ba91-813d-437a-b758-1b53fb3289ab
# ╟─847a53cd-a680-4c5a-9a5f-9b4ad5f6a202
# ╠═d7145ba9-9168-4716-91e2-9a268a752715
# ╠═6b04d4d9-abb9-48f4-ba38-9fed330563e1
# ╠═03b8a293-6ad3-4b2d-a0bb-5d896ba50cfc
# ╟─0ec8e74c-75c0-44d4-b995-690382c58691
# ╠═13a2ad81-a042-4269-b3d8-ede0638b4b3d
# ╠═b2be21e4-3a35-4be9-af77-c118c2d675dd
# ╟─589ec279-3d50-4def-a00e-c01e0f817719
# ╠═23d16f89-500f-4213-aa50-fa7548004676
# ╠═86b47439-ac69-4de1-92b2-a87ea4cbe616
# ╟─0da565ff-8cfb-44f0-b6dd-810521739a94
# ╠═04d65293-e48d-426d-9cbb-d795d58abe5a
# ╟─cc38553e-dd22-4fee-a71e-e691529bcf7c
# ╠═61330842-c78e-4208-baee-bc743505750b
# ╟─b5a1601b-d48d-4d40-8a2b-ff3084730a4d
# ╠═1592bcaf-08db-4bd7-be18-96797703eeca
# ╟─4c6cfecb-be6a-4f27-bc20-83016dada5d3
# ╠═27ae1e12-f33a-4442-b3cc-5605b6f65c33
# ╠═5cc8d730-b737-48c4-a0f7-3af717c05a81
# ╟─25bd604d-16cc-453f-99c5-c65e9036e442
# ╠═c0b545e3-eaac-44ff-8302-3d4f282302b3
# ╟─6d4cbc71-95b8-4945-b887-5456d09dc05c
