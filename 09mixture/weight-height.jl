### A Pluto.jl notebook ###
# v0.14.6

using Markdown
using InteractiveUtils

# ╔═╡ 3221b210-8716-11eb-08d0-d9254fad7c98
using CSV, DataFrames, Statistics, StatsPlots, Distributions, Turing, StatsFuns

# ╔═╡ 41098ec9-77dc-4ed0-aab9-04c221d36c54
md"# Mixture model -- human weight"

# ╔═╡ 3e354594-8716-11eb-1e34-1f733bf7932d
df = CSV.read("weight-height.csv", DataFrame)

# ╔═╡ 795e02e7-233e-424d-bd93-b4f5c9dd5ea2
md"## Data visualization"

# ╔═╡ 11921312-8718-11eb-0c6f-c3c312d8c9f0
function viz(values::Array{Float64, 1})
	μ = mean(values)
	σ = std(values)
	p = histogram(values, normalize=true, label="data")
	x = range(minimum(values), stop=maximum(values), length=100)
	plot!(p, x, pdf.(Normal(μ, σ), x), width=3, label="normal pdf")
	return p
end	

# ╔═╡ fa2e59dc-9812-481f-9793-3af4fc7cf166
md"Weights of males and femals separately seem to be described well by normal distributions."

# ╔═╡ 39cec4fe-871a-11eb-05e6-89d835d83388
let p = viz(df[in(["Male"]).(df.Gender), :Weight])
	xlabel!("male weight")
	p
end

# ╔═╡ b0cc2484-871a-11eb-3f4b-3752b8a7776c
let p = viz(df[in(["Female"]).(df.Gender), :Height])
	xlabel!(p, "female weight")
	savefig("female-weight.svg")
	p
end

# ╔═╡ c8b99c2e-a4af-4b8a-8ea1-761efc74602d
md"But taken together, the distribution is definitely not normal, even multimodal."

# ╔═╡ bd9fd2ca-871a-11eb-1ec0-6323a2d2e2c1
let p = viz(df[:, :Height])
	xlabel!("human weight")
	savefig("human-weight.svg")
	p
end

# ╔═╡ 30ccb67a-52a5-457b-9365-683cd4b2d1d8
md"## Hierarchical model

If we know the gender of each measurement, we can define a hierarchical model."

# ╔═╡ 43653461-8331-4a82-ba6e-e2fbc84843b3
@model function hierarchical(weights, males)
	μ0 ~ Normal(150, 100)
	σ0 ~ Gamma(1, 100)
	μm ~ Normal(μ0, σ0)
	μf ~ Normal(μ0, σ0)
	σ ~ Gamma(2, 50)
	for i in eachindex(weights)
		if males[i]
			weights[i] ~ Normal(μm, σ)
		else
			weights[i] ~ Normal(μf, σ)
		end
	end
end

# ╔═╡ 593a1ac3-edb3-4927-abd6-0b245a12cb2a
let chain = sample(hierarchical(df[:, :Weight], df[:, :Gender] .== "Male"), NUTS(), 1000)
	plot(chain)
end

# ╔═╡ f51271b9-b2f9-4ab0-997d-a2c1f0d9d96f
md"## Mixture model

However, it may happen that we do not know which measurements are taken for males and which one for females. Can we still model the population distribution? 
"

# ╔═╡ d3388b16-b127-46a5-b011-34c60aef01b4
@model function naive_discrete_mixture(weights)
	μ0 ~ Normal(150, 100)
	σ0 ~ Gamma(1, 100)
	μa ~ Normal(μ0, σ0)
	μb ~ Normal(μ0, σ0)
	σ ~ Gamma(2, 50)
	groupa = Vector{Bool}(undef, length(weights))
	for i in eachindex(weights)
		groupa[i] ~ Bernoulli(0.5)
		if groupa[i]
			weights[i] ~ Normal(μa, σ)
		else
			weights[i] ~ Normal(μb, σ)
		end
	end
end

# ╔═╡ 40d9398e-141d-4a61-a508-8b6520419190
md"Instead of creating a random variable for each observation, we can marginalize:"

# ╔═╡ 47fd1e35-8370-405d-aa19-fbe9f1e546a9
md"$$\Pr(y|\mu_a, \mu_b) = \Pr(\mu_a)\Pr(y|\mu_a) + \Pr(\mu_b)\Pr(y|\mu_b)$$"

# ╔═╡ c517ee17-eeb3-46d8-8845-36b2e21c777e
@model function discrete_mixture(weights)
	μ0 ~ Normal(150, 100)
	σ0 ~ Gamma(1, 100)
	μa ~ Normal(μ0, σ0)
	μb ~ Normal(μ0, σ0)
	σ ~ Gamma(2, 50)
	for i in eachindex(weights)
		Turing.acclogp!(_varinfo, logsumexp([
					logpdf(Normal(μa, σ), weights[i])
					logpdf(Normal(μb, σ), weights[i])]))
	end
end

# ╔═╡ 01be238e-3914-4582-bcd9-92f42042ae0f
let chain = sample(discrete_mixture(df[:, :Weight]), NUTS(), 1000)
	plot(chain)
end

# ╔═╡ 533b33dc-d356-432e-85a8-a8caf2ed8824
let chain = sample(discrete_mixture(rand(df[:, :Weight], 1000)), NUTS(), 1000)
	plot(chain)
end

# ╔═╡ 620b1e8a-fdae-45c8-90bd-79c081f9c597
md"We do not have to specify hyperparameters. A mixture model can be flat (without hyperparameters:"

# ╔═╡ 594c8ace-a6e9-4985-8df3-0a0c5bd7de47
@model function flat_discrete_mixture(weights)
	μa ~ Normal(150, 100)
	μb ~ Normal(150, 100)
	σ ~ Gamma(2, 50)
	for i in eachindex(weights)
		Turing.acclogp!(_varinfo, logsumexp([
					logpdf(Normal(μa, σ), weights[i])
					logpdf(Normal(μb, σ), weights[i])]))
	end
end

# ╔═╡ 63c68c3d-5825-4273-ab1f-e371e706d308
let chain = sample(flat_discrete_mixture(df[:, :Weight]), NUTS(), 1000)
	plot(chain)
end

# ╔═╡ 3881cf38-c8b4-42fc-b5e0-9ce521142c14
let chain = sample(flat_discrete_mixture(rand(df[:, :Weight], 1000)), NUTS(), 1000)
	plot(chain)
end

# ╔═╡ acda321a-9746-4cd5-af23-1786508343a5
md"The values of μa and μb do not have any particular order. We can see that the values can change places if we run more than a single chain. This is called 'label switching'"

# ╔═╡ 6f86e410-a98c-4eca-8fd3-61a8bf6d6d90
let chain = sample(discrete_mixture(df[:, :Weight]), NUTS(), MCMCThreads(), 1000, 3)
	plot(chain)
end

# ╔═╡ Cell order:
# ╠═41098ec9-77dc-4ed0-aab9-04c221d36c54
# ╠═3221b210-8716-11eb-08d0-d9254fad7c98
# ╠═3e354594-8716-11eb-1e34-1f733bf7932d
# ╟─795e02e7-233e-424d-bd93-b4f5c9dd5ea2
# ╠═11921312-8718-11eb-0c6f-c3c312d8c9f0
# ╟─fa2e59dc-9812-481f-9793-3af4fc7cf166
# ╠═39cec4fe-871a-11eb-05e6-89d835d83388
# ╠═b0cc2484-871a-11eb-3f4b-3752b8a7776c
# ╟─c8b99c2e-a4af-4b8a-8ea1-761efc74602d
# ╠═bd9fd2ca-871a-11eb-1ec0-6323a2d2e2c1
# ╟─30ccb67a-52a5-457b-9365-683cd4b2d1d8
# ╠═43653461-8331-4a82-ba6e-e2fbc84843b3
# ╠═593a1ac3-edb3-4927-abd6-0b245a12cb2a
# ╟─f51271b9-b2f9-4ab0-997d-a2c1f0d9d96f
# ╠═d3388b16-b127-46a5-b011-34c60aef01b4
# ╟─40d9398e-141d-4a61-a508-8b6520419190
# ╟─47fd1e35-8370-405d-aa19-fbe9f1e546a9
# ╠═c517ee17-eeb3-46d8-8845-36b2e21c777e
# ╠═01be238e-3914-4582-bcd9-92f42042ae0f
# ╠═533b33dc-d356-432e-85a8-a8caf2ed8824
# ╠═620b1e8a-fdae-45c8-90bd-79c081f9c597
# ╠═594c8ace-a6e9-4985-8df3-0a0c5bd7de47
# ╠═63c68c3d-5825-4273-ab1f-e371e706d308
# ╠═3881cf38-c8b4-42fc-b5e0-9ce521142c14
# ╟─acda321a-9746-4cd5-af23-1786508343a5
# ╠═6f86e410-a98c-4eca-8fd3-61a8bf6d6d90
