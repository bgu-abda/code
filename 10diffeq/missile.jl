### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# ╔═╡ 14bdc1e2-c2c5-11eb-3a46-ff199f60d2c1
using Turing, Distributions, StatsPlots

# ╔═╡ bed0cb8a-e0e4-430f-b852-5eb2579a6df7
md"# Ballistic trajectory

This case study solves an *inverse problem* of inferring initial velocity and angle of a ballistic trajectory given the distance to the target. Instead of using a closed form solution or custom method for solving inverse problems, we resort to Bayesian inference.
"

# ╔═╡ 4d6ec46b-4ab8-4fd7-9234-9d88c3055b25
md"## Model

We define the model abstractly, passing the shot simulator as a parameter. The simulator should return the distance covered by the missile given the initial velocity and angle."

# ╔═╡ 5f92d79c-e9ce-42ef-aa44-c74860afc573
@model function missile(shot, distance_to_target, dt=1)
	v ~ LogNormal(0, 10)
	α ~ Uniform(0, 49π/100)
	# x = shot(v, α, dt) 
	# x ~ Normal(distance_to_target , distance_to_target/10)

	distance_to_target ~ Normal(shot(v, α, dt), distance_to_target/10)
end

# ╔═╡ 389ed731-c33a-4693-9d5b-039760dceabb
md"## Moon shot

First, we consider the simplest case in which air resistance is ignored. Think of a missile launched on the moon, where there is no atmosphere.
"

# ╔═╡ a88c226e-2861-4bdd-8b8d-52b9560fe145
function moon_shot(v, α, dt=1)
	vx = v * cos(α)
	vy = v * sin(α)
	g = 9.8
	x, y = 0, 0
	n = 0
	while y >= 0 && n < 10000
		y += vy*dt
		x += vx*dt
		vy -= g*dt
		n += 1
	end
	return x
end

# ╔═╡ dedcecfc-ebd4-44c2-9d8a-2930497224c4
moon_chn = sample(missile(moon_shot, 100, 0.1), HMC(0.05, 50), 5000)[100:end]

# ╔═╡ 18fed26f-ba22-4146-8882-3d9c8e10d90a
plot(moon_chn)

# ╔═╡ bc35c2a8-cdf8-49ef-a9d5-057546f35b42
md" In addition to presenting raw inference results, we present the posterior in two forms:

* the 2-dimensional scatter plot of velocities and angle locations.
* the histogram of posterior predictive distances covered by the missile.
"

# ╔═╡ c2e33526-6f72-4e3a-860b-3b1582ab38c6
scatter(moon_chn[:v], moon_chn[:α], xlabel="v", ylabel="α", label=missing,
	alpha=0.25, xlim=(0, 200))

# ╔═╡ 45e8362a-c22c-4522-a010-4170234b9aca
histogram([moon_shot(moon_chn[:v][i], moon_chn[:α][i]) for i in 1:length(moon_chn)], label=missing, xlabel="distance", normalize=:density)

# ╔═╡ cc8dd5a5-e630-4958-9fc9-0f8e3c91d076
md"## Jupyter shot

Then, we simulate the shot on Jupyter (or any other planet) with thick atmopshere. Air resistance becomes a significant factor affecting the trajectory. To model atmosphere resistance, we use [Stokes drag](https://en.wikipedia.org/wiki/Projectile_motion#Trajectory_of_a_projectile_with_Stokes_drag) (resistance proportional to velocity).
"

# ╔═╡ 68a47d40-e95b-47b1-9fc3-201f066b996b
md"### Differential equations

$$\frac {dx} {dt} = v_x$$
$$\frac {dy} {dt} = v_y$$
$$\frac {dv_x} {dt} = - \mu v_x$$
$$\frac {dv_y} {dt} = -g - \mu v_y$$

### Initial conditions

$$v_x = v \cos(\alpha)$$
$$v_y = v \sin(\alpha)$$
"

# ╔═╡ ddcd5115-1591-4ae8-a988-2f61b5d6bf49
function jupyter_shot(v, α, dt=1)
	vx = v * cos(α)
	vy = v * sin(α)
	g = 9.8
	β = 0.01
	μ0 = 1.
	x, y = 0, 0
	n = 0 # make sure simulation finishes
	while y >= 0 && n < 10000
		y += vy*dt
		x += vx*dt
		vy -= g*dt
		μ = μ0*exp(-β*y) # viscosity decreases with elevation
		vx -= μ*vx*dt
		vy -= μ*vy*dt
		n += 1
	end
	return x
end

# ╔═╡ fec52f8b-314e-4d54-b485-09e9b483d50e
jupyter_chn = sample(missile(jupyter_shot, 100, 0.1), HMC(0.05, 50), 5000)[100:end]

# ╔═╡ 21011d21-6aa8-42f0-a111-3501571de3e0
plot(jupyter_chn)

# ╔═╡ 2c94c137-48b3-4cd0-9173-82112f7896e2
scatter(jupyter_chn[:v], jupyter_chn[:α], xlabel="v", ylabel="α", label=missing, alpha=0.25, xlim=(0, 500))

# ╔═╡ 381edcca-b6a5-4283-bb8d-32a871446326
histogram([jupyter_shot(jupyter_chn[:v][i], jupyter_chn[:α][i]) for i in 1:length(jupyter_chn)], label=missing, xlabel="distance")

# ╔═╡ Cell order:
# ╟─bed0cb8a-e0e4-430f-b852-5eb2579a6df7
# ╠═14bdc1e2-c2c5-11eb-3a46-ff199f60d2c1
# ╟─4d6ec46b-4ab8-4fd7-9234-9d88c3055b25
# ╠═5f92d79c-e9ce-42ef-aa44-c74860afc573
# ╟─389ed731-c33a-4693-9d5b-039760dceabb
# ╠═a88c226e-2861-4bdd-8b8d-52b9560fe145
# ╠═dedcecfc-ebd4-44c2-9d8a-2930497224c4
# ╠═18fed26f-ba22-4146-8882-3d9c8e10d90a
# ╟─bc35c2a8-cdf8-49ef-a9d5-057546f35b42
# ╠═c2e33526-6f72-4e3a-860b-3b1582ab38c6
# ╠═45e8362a-c22c-4522-a010-4170234b9aca
# ╟─cc8dd5a5-e630-4958-9fc9-0f8e3c91d076
# ╟─68a47d40-e95b-47b1-9fc3-201f066b996b
# ╠═ddcd5115-1591-4ae8-a988-2f61b5d6bf49
# ╠═fec52f8b-314e-4d54-b485-09e9b483d50e
# ╠═21011d21-6aa8-42f0-a111-3501571de3e0
# ╠═2c94c137-48b3-4cd0-9173-82112f7896e2
# ╠═381edcca-b6a5-4283-bb8d-32a871446326
