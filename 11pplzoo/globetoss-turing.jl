### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# ╔═╡ 9c2725e8-ad9d-11eb-2c42-13b0c0814258
using Turing, Distributions, Statistics, StatsPlots, Random

# ╔═╡ ef964bc1-8a69-415e-9457-8899e550d3f4
md"# Globe toss

Beta-Bernoulli model: inference and analysis in _Turing_.
"

# ╔═╡ d54e2e55-4a1c-4549-853e-5b8574fe0605
md"## Data

A set of outcomes of tossing a globe and sticking the finger in it randomly. The output of each experiment is either 0 (land) or 1 (water). We want to infer the probability of touching either land or water.
"

# ╔═╡ 162dcc45-359b-49c7-b667-345812913670
begin
	L = 0
	W = 1
	data = [L, L, W, W, W, W, W, L, L, L, L, L, W, W, W, W, W, W, W, W]
	# data = [i % 2 for i in 1:20]
	# data = rand(0:1, 20)
end

# ╔═╡ a32f9277-04b7-4f7c-89a8-915fc34a27bc
md"## Model

The model imposes Beta(1, 1) prior on the probability of water $p$ and conditions the distribution of $p$ on observations.
"

# ╔═╡ e847daf0-746e-43aa-bfbd-dc157c9629d0
@model function globetoss(y::Array{Int,1})
	p ~ Beta(1, 1)
	for i = eachindex(y)
		y[i] ~ Bernoulli(p)
	end
end

# ╔═╡ 23cc2f0c-1644-4681-9af0-bfebb0c485bc
md"## Inference

since the latent state of the model is differentiable, we run the MCMC inference using HMC. We represent the posterior as a histogram of the posterior probability distribution of $p$.
"

# ╔═╡ 04b65ea9-2721-439e-9b4a-1283f31d3ff1
begin
	chain = sample(globetoss(data), HMC(0.1, 10), 10000)
	histogram(chain[:p], xlabel="p", xlim=(0, 1), seriestype=:pdf, label=missing)
end

# ╔═╡ 83183a46-4ad8-4022-94eb-ab9ebf2e19f7
md"## Checking

We want to check that the model is adequate for the data, in particular for predicting the outcome of the next toss. Our checks will be based on comparing the predictive posterior with the data.
"

# ╔═╡ 5b4451c7-ecbc-4ea6-a064-0e3bcd4bde17
function simulate_globetoss(chain, size=1000)
	p = sample(chain[:p], size)
	tosses = Array{Int, 2}(undef, size, length(data))
	for i in 1:size
		for j in 1:length(data)
			tosses[i, j] = rand(Bernoulli(p[i]))
		end
	end
	tosses
end

# ╔═╡ 3fa28bb3-c751-4168-9a45-c187bda0a3ad
md"Since we based our model on the exchangeability assumption, we will check whether the subsequent globe tosses are indeed independent. For that, we'll count the number of switch from L to W and vice versa in the sequence.
"

# ╔═╡ b6554bc6-e972-430a-8e97-b743cf8e0e10
function count_switches(data)
	count = 0
	for i = 1:length(data)-1
		if data[i] != data[i+1]
			count += 1
		end
	end
	count
end

# ╔═╡ 4db5d1cb-cec4-4255-805c-1b72f2156b6b
md"To check the exchangeability assumption, we sample from the predictive posterior multiple simulated data sets, and compare the number of switches in the simulated data sets (blue in the plot below) to the number of switches in the real data set (the red vertical line). 
"

# ╔═╡ db4dddfa-8e20-49fb-84f2-b29fd85ece8f
let N=1000
	switches = Array{Int}(undef, N)
	simulations = simulate_globetoss(chain, N)
	for i in 1:N
		switches[i] = count_switches(simulations[i, :])
	end
	switch_data = count_switches(data)
	p_value = length(switches[switches .< switch_data])/ length(switches)
	histogram(switches, label="simulated", xlabel="p_value = $p_value")
	vline!([switch_data], lw=8, label="observed")
end

# ╔═╡ ea3fc457-f6ff-470b-ac66-9c29b48113c8
md"If you see what I do, there are much fewer switches in the real data set than in the simulated ones. If we are going to use the model to predict the next globe toss outcome (rather than estimate the percentage of water on the globe), we probably need a better model.
"

# ╔═╡ e21ca6af-0c84-4b14-92cf-89339d21d81f
md"## Accounting for dependencies"

# ╔═╡ 7e5a0e88-164f-48c5-9d94-a58241450327
md"A model better suited for prediction of the next toss outcome should account for the ‘stickiness’ of tosses: the probability of each particular outcome depends on both the average probability of the outcome and on the previous outcome. Hence, the model will have two parameters, $p$ — the average probability of water, and $q$ — the probability that the next toss outcome will be the same as the last one."

# ╔═╡ 17f76f21-e8bb-4192-bfeb-b465aedef7ec
md"### Model"

# ╔═╡ 4508d1ac-7a69-4cbd-ac76-8d825477df50
@model function globetoss_walk(y::Array{Int,1})
	p ~ Beta(1, 1)
	q ~ Beta(1, 1)
	y0 ~ Bernoulli(p)
	yprev = y0
	for i = eachindex(y)
		if yprev == 1
			y[i] ~ Bernoulli(q+(1-q)*p)
		else
			y[i] ~ Bernoulli((1-q)*p)
		end
		yprev = y[i]
	end
end

# ╔═╡ 665856d4-00ff-412a-8439-6d0f58fbe8c1
md"### Inference"

# ╔═╡ 9c794942-3aeb-4c70-a7c5-22e334515d59
begin
	chain_walk = sample(globetoss_walk(data), Gibbs(HMC(0.1, 10, :p, :q), MH(:y0)), 10000)
	plt = plot(layout=2)
	histogram!(plt, chain_walk[:p], xlabel="p", xlim=(0, 1), seriestype=:pdf, label=missing, subplot=1)
	histogram!(plt, chain_walk[:q], xlabel="q", xlim=(0, 1), seriestype=:pdf, label=missing, subplot=2)
end

# ╔═╡ 8a9229f3-0bc2-4fb7-9901-4310c89a4aea
md"### Checking"

# ╔═╡ c3aceec1-dbfc-4326-a91c-4bcaf8f83b66
md"For the new model, we need a new simulator, sampling sequentially."

# ╔═╡ 6c5728a3-7778-4940-9c61-0c92136e8b12
function simulate_globetoss_walk(chain, size=1000)
	y0 = sample(chain[:y0], size)
	p = sample(chain[:p], size)
	q = sample(chain[:q], size)
	tosses = Array{Int, 2}(undef, size, length(data))
	for i in 1:size
		yprev = (y0[i] == 1)
		for j in 1:length(data)
			if yprev
				tosses[i, j] = rand(Bernoulli(q[i] + (1 - q[i])*p[i]))
			else
				tosses[i, j] = rand(Bernoulli((1 - q[i])*p[i]))
			end
			yprev = tosses[i, j] == 1
		end
	end
	tosses
end

# ╔═╡ 6c23511b-6c44-4ba6-a001-fa5462245021
let N=1000
	switches = Array{Int}(undef, N)
	simulations = simulate_globetoss_walk(chain_walk, N)
	for i in 1:N
		switches[i] = count_switches(simulations[i, :])
	end
	switch_data = count_switches(data)
	p_value = length(switches[switches .< switch_data])/ length(switches)
	histogram(switches, label="simulated", xlabel="p_value = $p_value")
	vline!([switch_data], lw=8, label="observed")
end

# ╔═╡ ba76bb4b-b9c4-4d65-b614-7ba688091026
md"Now the number of simulated switches is much closer to the number of real ones, so good chances are that the revised model will predict future tosses better."

# ╔═╡ Cell order:
# ╟─ef964bc1-8a69-415e-9457-8899e550d3f4
# ╠═9c2725e8-ad9d-11eb-2c42-13b0c0814258
# ╟─d54e2e55-4a1c-4549-853e-5b8574fe0605
# ╠═162dcc45-359b-49c7-b667-345812913670
# ╟─a32f9277-04b7-4f7c-89a8-915fc34a27bc
# ╠═e847daf0-746e-43aa-bfbd-dc157c9629d0
# ╟─23cc2f0c-1644-4681-9af0-bfebb0c485bc
# ╠═04b65ea9-2721-439e-9b4a-1283f31d3ff1
# ╟─83183a46-4ad8-4022-94eb-ab9ebf2e19f7
# ╠═5b4451c7-ecbc-4ea6-a064-0e3bcd4bde17
# ╟─3fa28bb3-c751-4168-9a45-c187bda0a3ad
# ╠═b6554bc6-e972-430a-8e97-b743cf8e0e10
# ╟─4db5d1cb-cec4-4255-805c-1b72f2156b6b
# ╠═db4dddfa-8e20-49fb-84f2-b29fd85ece8f
# ╟─ea3fc457-f6ff-470b-ac66-9c29b48113c8
# ╟─e21ca6af-0c84-4b14-92cf-89339d21d81f
# ╟─7e5a0e88-164f-48c5-9d94-a58241450327
# ╠═17f76f21-e8bb-4192-bfeb-b465aedef7ec
# ╠═4508d1ac-7a69-4cbd-ac76-8d825477df50
# ╟─665856d4-00ff-412a-8439-6d0f58fbe8c1
# ╠═9c794942-3aeb-4c70-a7c5-22e334515d59
# ╟─8a9229f3-0bc2-4fb7-9901-4310c89a4aea
# ╟─c3aceec1-dbfc-4326-a91c-4bcaf8f83b66
# ╠═6c5728a3-7778-4940-9c61-0c92136e8b12
# ╠═6c23511b-6c44-4ba6-a001-fa5462245021
# ╟─ba76bb4b-b9c4-4d65-b614-7ba688091026
